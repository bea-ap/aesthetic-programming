class Skater { //Initializing the skater object
   constructor(){
    // Initializing skater properties
    this.r = 200; // Radius of skater's circle
    this.x = this.r; // X-coordinate of skater's position (starting position)
    this.y = height - this.r; // Y-coordinate of skater's position (starting position)
    this.speed = 0; // Initial speed of skater (when not jumping)
    this.gravity = 1; // Gravity affecting skater's fall
    this.crossedTrash = false; // Indicates if skater crossed trash
  }

  jump(){
    // Function for skater's jump
    if (this.y == height - this.r){
      this.speed = -30; // Speed set to negative value to make the skater jump upwards
    }
  }
  
  hits(trash) {
    // Function to create collision between skater and a trash can
    // Computing positions and sizes of both objects and for circular collision
    let x1 = this.x + this.r * 0.05; 
    let y1 = this.y + this.r * 0.05; 
    let x2 = trash.x + trash.r * 0.05; 
    let y2 = trash.y + trash.r * 0.05; 

    // Using p5.js function collideCircleCircle() for circular collision
    return collideCircleCircle(x1, y1, this.r, x2, y2, trash.r);
  }

  move(){
    // Function to control skater's movement and gravity effect
    this.y += this.speed; // Updating skater's y-coordinate based on its speed
    this.speed += this.gravity; // Gravity to skater's speed
    this.y = constrain(this.y, 0, height - this.r); // Ensuring skater does not move outside the canvas
  }

  show(){
    // Function to display skater's image on the screen
    image(skatergif, this.x, this.y, this.r, this.r); // Drawing skater's image on screen at its current position and size
  }
} 
