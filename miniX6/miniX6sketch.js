//Deaclaring variables
let skatergif;
let trashImg;
let backgroundgif;
let trashcans = [];
let score = 0;
let song; 
let errorSound;
let gameOver = false; // Global variabel to track the ending of the game 
let gameState = "start"; // Global variabel to track the state of the game

function preload(){
  skatergif = loadImage('skater.gif');
  trashImg = loadImage('trash.png');
  backgroundgif = loadImage('city.gif');
  song = loadSound('Kanye.mp3');
  errorSound = loadSound('error.mp3');
}

function setup(){
  createCanvas(windowWidth, windowHeight);
  skater = new Skater();
}

function startScreen() {
  stroke(1,2,3);
  strokeWeight(5);
  textSize(50);
  fill(255);
  textAlign(CENTER);
  text("Press SPACE to start", width / 2, height / 2);
}

function keyPressed(){
  if (gameState === "start" && key == ' ') { // If the game is in the start state and the spacebar is pressed
    gameState = "play"; // Change the game state to play
    song.play(); // Start background music
  }
  if (gameState === "play" && key == ' ') { // If the game is in the play state and the spacebar is pressed
    skater.jump(); // Make the skater jump
  }
  if (gameState === "gameover" && key == ' ') { // If the game is over and the spacebar is pressed
    resetGame(); // Reset the game
  }
}

function resetGame() {
  // Reset all game variables to their initial values
  trashcans = [];
  score = 0;
  gameOver = false;
  gameState = "start"; // Switch back to the start state
  loop(); // Resume game loop
}

function draw(){
  background(backgroundgif);

  if (gameState === "start") { // If the game is in the start state
    startScreen(); // Show the start screen
  } else if (gameState === "play") { // If the game is in the play state
    // Game logic continues here
    if (!gameOver) {
      if (random(1) < 0.005) {
        trashcans.push(new Trash());
      }
      
      for (let t of trashcans){
        t.move();
        t.show();
        
        if (skater.hits(t)){
          textSize(100);
          textAlign(CENTER);
          fill(217, 33, 33);
          text('GAME OVER', width / 2, height / 2);
          textSize(32);
          fill(255);
          text("Score: " + score, width / 2, height / 2 + 50);
          text("Press SPACE to play again", width / 2, height / 2 + 100); // Show "Press space to play again" message
          noLoop();
          errorSound.play();
          song.stop(); // Stop the background song
          gameOver = true; // Indicate that the game is over
          gameState = "gameover"; // Switch game state to gameover
        }
        if (t.x + t.r < skater.x && !t.crossed) {
          score++;
          t.crossed = true;
        }
      }
      
      textSize(32);
      fill(255);
      stroke(1, 2, 3);
      text("Score: " + score, 100, 70);
      
      skater.show();
      skater.move();
    }
  } else if (gameState === "gameover") { // If the game is over
    textSize(50);
    fill(255);
    textAlign(CENTER);
    text("Press space to play again", width / 2, height / 2); // Show "Press space to play again" message
  }
}
