class Trash { // Trash object initialization
  constructor() {
  this.r = 125; // Radius of trash can
  this.x = width; // Initial x-coordinate of trash can 
  this.y = height - this.r; // Initial y-coordinate of trash can (near bottom of the canvas)
  this.crossed = false; // Initialize crossed state 
}

move(){
  this.x -= 10; // Move trash can towards left by 10 pixels in each frame
}

show(){
  image(trashImg, this.x, this.y, this.r, this.r); // Display trash can image at its current position and size
}  
}