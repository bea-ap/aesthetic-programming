# MiniX6: Skater game
![](miniX6.webm)
<br>
Run the code [here](https://bea-ap.gitlab.io/aesthetic-programming/miniX6/index.html)
<br>
View the main code [here](https://gitlab.com/bea-ap/aesthetic-programming/-/blob/main/miniX6/miniX6sketch.js)
<br>
View skater code [here](https://gitlab.com/bea-ap/aesthetic-programming/-/blob/main/miniX6/skater.js)
<br>
View trash code [here](https://gitlab.com/bea-ap/aesthetic-programming/-/blob/main/miniX6/trash.js)

## Questions

**How does/do your game/game objects work?**

For my miniX6 I have chosen to program a game inspired by the Dinosaur game by Google Chrome, where a dinosaur has to jump over cacti, and if the dinosaur collides with a cactus, the game ends. In my game I have two main objects which are trash cans and a skater instead of cacti and a Dinosaur. My game works the same way as the Dinosour game, where points are given every time the skater jumps over a trash can using the space button and if the skater collides with a trash can the game is over.

**Describe how you program the objects and their related attributes, and the
methods in your game**

There are two main objects in my game; the skater and the trashcans.

I have made the Skater Object with the following attributes:
`this.r`: The radius of the skater.
`this.x, this.y`: The coordinates of the skater's position.
It also has `this.speed` which represents the speed of the skater, when the skater jumps and `this.gravity` to control how strong the force of gravity must be when the skater jumps.

I use the following methids for the skater object:
`constructor()`: Initializes the skater object with default attributes.
`jump()`: Makes the skater jump by changing its vertical speed.
`hits(trash)`: Checks for collision between the skater and a trash can.
`move()`: Updates the skater's position based on its speed and gravity.
`show()`: shows the skater on the screen.

The trash Object also has `this.r`and `this.x, this.y` as attributes. 
Furthermore it has `this.crossed`: which is a boolean indicating whether the skater has passed the trash can.

For the trashcan object I also use the methods: `constructor`, `move`and `show`.

**Connect your game project to a wider cultural context, and think of an example
to describe how complex details and operations are being “abstracted”?**

The dinosaur game that inspired this miniX can be found on Google Chrome when there's no internet connection. It works as a distraction to pass the time while waiting for the connection to be restored. I find this implementation very smart and more engaging than simply staring at a loading throbber. However, it is also interesting that Chrome has found it necessary to develop a whole game to keep users entertained. It highlights the contemporary need for everything to go fast to prevent boredome and impatience. I think this game is a good example of a simplification of complex technical details. The player is entertained without needing to understand the underlying complexities. This abstraction makes the game fun and easy for a wider audience to play.

### Referrences
https://www.youtube.com/watch?v=l0HoJHc-63Q

https://github.com/bmoren/p5.collide2D

Soon Winnie & Cox, Geoff, "Object Abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164 - chapter 6.

