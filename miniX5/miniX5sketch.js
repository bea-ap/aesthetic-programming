function setup() {
    createCanvas(windowWidth, windowHeight);
    noFill();
    stroke(0);
    strokeWeight(2);
  }
  
  function draw() {
    background(240);
  
    // defining noise level and scale.
    let noiseLevel = 1000;
    let noiseScale = 0.007;
  
    // Iterate from left to right.
    for (let x = 0; x < width; x += 6) {
      // Scaling the input coordinates.
      let nx = noiseScale * x;
      let nt = noiseScale * frameCount;
  
      // Computing the noise value.
      let y = noiseLevel * noise(nx, nt);
  
      // Define the gradient color based on the line's position.
      let from = color(255, 180, 191); // Start color for the gradient
      let to = color(119, 158, 203); // End color for gradient
      let gradientColor = lerpColor(from, to, x / width); // Lerping colors based on the x-position
  
      // line color
      stroke(gradientColor);
  
      // defining line thickness based on line's position
      let weight;
      if (x < width / 3) {
        weight = lerp(6, 2, x / (width / 3)); // Interpoles between 6 and 2 pixels in the first third
      } else if (x < 2 * width / 3) {
        weight = lerp(2, 6, (x - width / 3) / (width / 3)); // Interpoles between 2 og 6 pixels in the second third
      } else {
        weight = lerp(6, 2, (x - 2 * width / 3) / (width / 3)); // Interpoles between 6 og 2 pixels in the last third
      }
  
      // line thickness
      strokeWeight(weight);
  
      // line
      line(x, height, x, height - y);
    }
  }
  