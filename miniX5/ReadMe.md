# MiniX5: A Generative Program
![](miniX5-video.webm)
<br>
Run the code [here](https://bea-ap.gitlab.io/aesthetic-programming/miniX5/index.html)
<br>
View the code [here](https://gitlab.com/bea-ap/aesthetic-programming/-/blob/main/miniX5/miniX5sketch.js?ref_type=heads)

## Questions

**What are the rules in your generative program? Describe how your program
performs over time? How do the rules produce emergent behavior?**

My first rule is defined as:

```
   for (let x = 0; x < width; x += 6) {
```
This rule is also a loop that iterates over values of 'x' starting from 0, incrementing by 6 each time, until 'x' reaches a value less than 'width'.

For my second rule I have used a conditional statement to determine the thickness of the lines depending on which third part of the screen the lines are in:
```
      if (x < width / 3) {
        weight = lerp(6, 2, x / (width / 3)); 
      } else if (x < 2 * width / 3) {
        weight = lerp(2, 6, (x - width / 3) / (width / 3)); 
      } else {
        weight = lerp(6, 2, (x - 2 * width / 3) / (width / 3));
```
  When x is in the first third of the screen, the weight of the line is assigned as a linear combination between 6 and 2 pixels.
  When x is in the second third, the weight of the line is assigned as a linear combination between 2 and 6 pixels.
  When x is in the last third, the weight is assigned as a linear combination between 6 and 2 pixels.

my program is fairly constant, nothing much different happens over time, apart from the fact that the position of the lines constantly varies because of the use of `noise()`. The rules above create emergent behavior in such a way that they have a great impact on the visuals of my program, my program would have looked very different without these rules.

**Draw upon the assigned reading, how does this MiniX help you to understand the
idea of “auto-generator” (e.g. levels of control, autonomy, love and care via
rules)? Do you have any further thoughts on the theme of this chapter?**

I find working with generative art very interesting and engaging. I am fascinated by the ability to create aesthetically pleasing programs that operate autonomously simply by implementing basic rules in the code. It has also given a broader understanding of how generative programs are used and created in more professional contexts on websites etc.

### Referrences
https://p5js.org/reference/#/p5/noise

Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142 
