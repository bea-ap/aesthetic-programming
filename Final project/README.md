## FACE CAPTURE 
### Final Project ReadMe - Group 8

![](Finalproject.png)

[**Click to view the RunMe**](https://bea-ap.gitlab.io/aesthetic-programming/Final%20project/index.html)
<br>
[**Click to view the code**](https://gitlab.com/bea-ap/aesthetic-programming/-/blob/main/Final%20project/finalXsketch.js)

**Characters: 15.762**

**About the program**

We have made a program titled 'Face Capture'. The program consists of a face tracker that scans the face of the person in front of the camera and makes assumptions about the person based on their face. It works as follows; when you open the program, a button appears at the top of the screen with the text: 'Who are you?'. When the button is pressed, a square line is drawn around the detected face. From this face scan, the exact RGB color in the center of the square is recorded, which constitutes the exact skin color at the registered point of the detected face. We have implemented a location API that registers the person's precise location, and additionally, assumptions are made about the person's net worth, criminal record, presumed age and days left until you die. These four values are displayed in a way that all possible values run in a loop for two seconds before the final assumed values are shown. This creates a throbber effect, indicating that something is about to happen and the program is processing. All the final values, based on assumptions of the detected face, including location and rgb color are displayed next to the person's face. After this, the program ends, however the user can choose to run it again by pressing the button and new assumed values are generated.
 
Our program explores the topic of data capture and ‘datafication’, this involves the ways in which our data is constantly monitored, collected, and analyzed: 

>"This term — a contraction of data and commodification — refers to the ways in which all aspects of
our life seem to be turned into data which is subsequently transferred into information which is then monetized" (Soon & Cox, 2020 p. 99).
>

It explores how this data is used by various entities, such as corporations and governments, to make decisions about individuals. Additionally, the topic addresses the potential consequences of data capture, including issues related to privacy, surveillance, data security, and the broader societal impacts (Soon & Cox, 2020). The program aims to shed light on these aspects and provoke thought about the implications of living in a futuristic data-driven world. This theme will be elaborated further through this ReadMe.

**Flowchart**

Our flowchart helps us to visualize the technical aspects of the code, it provides a good overview and contributes to a better understanding of it. In order to create each step in the flowchart, we must understand what exactly is happening in the code. To structure it a bit, we have divided it into the three different parts, which consists of: Set-up, if-statements and Visual arrangement. 

In the Set-up part, we are defining our variables, setting up the canvas, face-tracker etc. 

In the if-statements part, our two main if-statements in the code are represented. The first draws a square/rectangle around the detected face, if true. The other if-statement records the RGB value in the center of the rectangle, if true.

The visual arrangement part, is where the visual aspects of the code are displayed. There's an input saying 'button is pressed' and if the button is pressed, all of the different functions in the code are being called, wherafter the final values are displayed and the program ends (see appendix for the full flowchart).

**Set-up:**

![](Flowchart1.png)

**If-statements:**
![](Flowchart2.png)

**Visual arrangement:**
![](Flowchart3.png)

**Themes**

Our project addressed the theme of data collection and surveillance capitalism. Our project involves analyzing a person’s face, and judging them. The inspiration for this comes from science fiction works such as “Black Mirror,” (Béat, 2019)  but we also drew inspiration from the real world. The Chinese government in recent years has been in the forefront in terms of surveillance, and incorporating it into not only their economy, but also their political landscape (Yang, 2022).

The Chinese government has over 700 million surveillance cameras, meaning that there is one camera for every 2 people (Sharma, 2023). The information gathered from these camera feeds, alongside information the Chinese government can legally collect from all Chinese companies, such as WeChat and Alibaba Express, allows them to create accurate and constantly updating profiles of Chinese citizens, with it all culminating in a social credit score system (Ollier-Malaterre, 2024). In essence, this surveillance and the social credit system, allows for regulation and the application of executive power, or “punishment.” (Ollier-Malaterre, 2024). If a Chinese citizen jaywalks, it will be seen by a camera, which then lowers your social credit score, which may result in a more expensive bus ticket, or even that you have a longer wait time at the hospital (Bartsch et al., n.d). We tried to simulate this sort of, overarching surveillance historically seen in fiction, and in more recent years seen in real life, by creating a program that “scans” your face, and based on the scan judges you. It is meant to be unsettling, and even politically incorrect.

The Cambridge Analytica scandal stands as a stark reminder of what data collection can result in today, and it gives an idea of what the future may look like if data collection further escalates (Cadwalladr et al., 2018). The Cambridge Analytica scandal involved the company, Cambridge Analytica, buying the Facebook user information of more than 50 million Facebook users (Cadwalladr et al., 2018). This information was then used to create detailed user profiles of these people. These user profiles were then sold to Trump’s 2016 political campaign, where these profiles were used to target individuals who were statistically likely to be susceptible to Trump’s message (Rehman, 2019).

If the world we currently live in, is one where your personal data can be illegally collected, and used to target you for political gain, it is very scary to think of where we may end up in the future. Our project is meant to show what the future may look like if data collection isn’t further regulated.

**Cultural aspects**

The idea of a program that scans and judges faces brings up issues of bias, discrimination and the ethical use of surveillance technologies. In the real world, facial recognition technology has been criticized for its potential to perpetuate racial and gender biases (Najibi, 2020). Our project “guesses” your net worth, as well as your criminal record, which can be incredibly provocative, and seem very biased, even though the data shown is random. We also get into the concept of social control, justice and punishment by way of the criminal record randomizer. 

We live in a society where talking about, and in particular, judging based on race is rightfully very taboo, but the fact is, that corporations and countries alike use this information to judge us, no matter how racist and prejudiced it is. This is why we chose to represent this in our program, because it in some cases already is, and for most may be the future of surveillance and governance.

The project can be seen as a provocative and critical art piece, where we demonstrate the problems a surveillance state may create. That is the fall of democracy, where you can become a second class citizen based on arbitrary judgements, and the loss of privacy.

**Technical aspects**

To gain a better understanding of the technical aspects of our project we will explain how our code works and how they intersect with the cultural aspects. The program uses a *clmtrackr* library for JavaScript, that detects a precise position of facial features through the users’ webcam. The *tracker.init()* method initializes the library, setting up the facial tracking model, and *tracker.start(capture.elt)*, begins the face detection when the webcam starts. Once a face is detected, the *clmtrackr* library will identify the positions of various facial landmarks through the *getCurrentPosition()* function. This function returns an array of coordinates representing key points on the user's face, such as the eyes, nose, and mouth (Clmtrackr.js). We then use these facial landmarks to draw a rectangle around the user's face by calculating the minimum and maximum x- and y-coordinates of the detected landmarks. 

The webcam feed is captured using p5.js’s *createCapture(VIDEO)* function, which initializes the video capture element. We set the video size with *capture.size(windowWidth, windowHeight);* so it matches the browsers' dimensions. The program runs in real time, continuously analyzing video feed from the webcam and tracking for faces. We flip the video feed using *translate(width, 0); scale(-1, 1);* to create a mirror effect, aligning the reflection with how users see themselves in a mirror. 

As previously mentioned, we use an API to get the users’ location based on their IP-address. The URL for the API is stored as a constant variable named *locationAPI*. With the *getLocationData()* function we request JSON-formatted data with the function *fetch*. After receiving the response from our request, we extract the data into a string containing the country, city, latitude and longitude, stored in the *LocationInfo* variable. In the end, this location info is displayed in the *draw()* function, on the right side of the detected face, together with the other information. 

Other than the location, the RGB value is the only non-randomized value. This color value is obtained by capturing the pixel at the center of the face-framing rectangle using the function *capture.get()*. Initially, the *scanRGB* variable is set to false. However, when the *startScan()* function is triggered by pressing the button, *scanRGB* switches to true, initiating the scanning process. After one scan, *scanRGB* is reset to false, preventing further scanning. This ensures that the RGB value is captured only once when prompted by the user.

The other values, like age, criminal record, net worth, and days until you die, are randomized. The *generateRandomAge()* function generates a random age between 17 and 86. Each time the function is called, it generates a random age, which happens when the button is pressed. The other three values generate a random value from a predefined list of values from an array. These functions simulate the generation of demographic and socioeconomic data based on random probabilities, which adds variability and realism to the assumptions made by the program about the detected face. 

Lastly, we decided to add a function called *showPossibleOutcomes()*, which is responsible for displaying possible outcomes of the randomized values one by one before it stops on one random value. It iterates through the *possibleOutcomes* array for 2 seconds, with an interval of 50 milliseconds for each value. This effect is supposed to indicate that the process the user initiated has started, providing feedback that makes it more engaging. Additionally, it shows what goes on behind the computer and provides transparency into the underlying calculations.

**The artefact as critial work**

Our core idea has been to capture personal data and present it back to the user, both accurate data and fictional data. It is intended to serve as a critique of the pervasive nature of data collection and privacy invasion in modern society. By blending accurate data (RGB value, location) with fabricated information (age, criminal record, net worth), the artifact highlights the ease with which data can be manipulated, raising awareness about the reliability and ethical implications of data usage. The combination of real and random data elements is designed to unsettle the user, making them question the accuracy and sources of data presented to them. By accurately identifying their location, the artifact creates a sense of intrusion and vulnerability, which is central to its critical message. This emotional response is key in prompting users to reflect on their digital footprint and the broader implications of personal data harvesting. The sci-fi aspect, where exaggerated or improbable data is presented, serves as a satirical commentary on the dystopian potential of current technological trends. This exaggeration forces users to confront the potential extremes of data collection and misuse, serving as a warning about the trajectory of current practices.  

Overall, the artifact can be considered a critical work because it effectively uses technology to provoke thought and discussion about important social issues. Its ability to blend fact with fiction, create emotional responses, and engage users in an interactive critique all contribute to its effectiveness as a piece of critical art. By pushing the boundaries of data capture to an extreme, it serves as a powerful reminder of the potential consequences of unchecked data collection and surveillance. 
It can be compared to several other critical data artworks, like “LAUREN” by Lauren McCarthy, which we have found inspirational. Both our project and "LAUREN" engage deeply with themes of surveillance and data privacy. "LAUREN" critiques the intimate and invasive nature of AI assistants like Amazon's Alexa, with the twist that the AI is controlled by the artist herself. This setup explores the boundaries of personal privacy and consent, revealing the human element behind surveillance technologies​​.
Similarly, our project captures user images and provides a mix of real and randomized data, prompting users to reflect on the accuracy and ethics of data collection. By doing so, it highlights the often invisible processes of data surveillance and the potential for misuse, making these processes tangible and encouraging critical reflection.

"LAUREN" questions the trust users place in AI and digital assistants, revealing how easily these systems can be manipulated. Our project parallels this by showcasing how personal data can be misrepresented, thus critiquing the reliability of automated data labeling systems. If our project had used AI to generate user data labels, it would have similarly exposed the misleading nature of such technologies.

Ada Ada Ada's "in transitu" project further emphasizes the critique of algorithmic systems. By weekly posting bare-chested images of herself during her gender transition on Instagram, Ada Ada Ada challenges the platform's moderation rules and explores the biases in gender recognition algorithms. The project raises questions about when a chest is considered feminine enough to be censored, thus exposing the flaws and biases in these algorithmic determinations​. 

All three projects use personal data to critique and expose the flaws in modern technology. "LAUREN" uses real-time interaction to reveal the human element in AI surveillance, our project uses real and fictional data to provoke thought about data accuracy and privacy, and "in transitu" uses personal imagery to challenge algorithmic biases in gender recognition and social media censorship.

In conclusion, the comparison between our project, "LAUREN" by Lauren McCarthy, and Ada Ada Ada's "in transitu" effectively demonstrates how critical data art can be used to engage with and critique issues of surveillance, privacy, and the reliability of algorithmic technologies. Each project uniquely utilizes personal data to make the often invisible processes of data collection and surveillance visible and tangible, prompting critical reflection and dialogue about the implications of these technologies in our lives. Our project also works as a critique of itself, as it uses its own mechanisms to highlight and critique the flaws and ethical issues in data surveillance and manipulation. ​​This self-critique is a defining feature of critical art, making our project a contribution to the existing artworks in the field. 

### Bibliography
---

- Ada Ada Ada — in transitu. (n.d.). https://ada-ada-ada.art/projects/in-transitu
- Bartsch, B. & Gottske, M. (n.d). “CHINA’S SOCIAL CREDIT SYSTEM”. Bertelsmann Stiftung. https://www.bertelsmann-stiftung.de/fileadmin/files/aam/Asia-Book_A_03_China_Social_Credit_System.pdf
- Béat, E.L. (2019) “A submersion into Black Mirror’s Nosedive”. MARBLE. https://doi.org/10.26481/marble.2019.v2.737
- Cadwalladr, C. & Graham-Harrison, E. (2018). “Revealed: 50 million Facebook profiles harvested for 	Cambridge Analytica in major data breach”. The Guardian. https://www.theguardian.com/news/2018/mar/17/cambridge-analytica-facebook-influence-us-election
- Clmtrackr.js, library reference. Retrieved 03-06-24: https://www.auduno.com/clmtrackr/docs/reference.html
- LAUREN — Lauren Lee McCarthy. (n.d.). https://lauren-mccarthy.com/LAUREN
- Najibi, A. (2020). “Radical Discrimination in Face Recognition Technology”. HARVARD Kenneth C. Griffin Graduate School of Arts and Sciences. https://sitn.hms.harvard.edu/flash/2020/racial-discrimination-in-face-recognition-technology/
- Ollier-Malaterre, A. (2024). “Digital surveillance is omnipresent in China. Here’s how the citizens are coping”. The Conversation. https://theconversation.com/digital-surveillance-is-omnipresent-in-china-heres-how-citizens-are-coping-225628
- Rehman, I.U. (2019). “Facebook-Cambridge Analytica data harvesting: What you need to know”. University of Nebraska-Lincoln. https://core.ac.uk/outputs/220153793/?utm_source=pdf&utm_medium=banner&utm_campaign=pdf-decoration-v1
- Sharma, U. (2023). “Big Brother is watching: China has one surveillance camera for every 2 citizens!”. First Post. https://www.firstpost.com/world/big-brother-is-watching-china-has-one-surveillance-camera-for-every-2-citizens-12380062.html
- Soon, Winnie & Cox, Geoff (2020). "Data Capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, pp. 97-120 (Chapter 4)
- Yang, Z. (2022). “The Chinese surveillance state proves that the idea of privacy is more “malleable” than you’d expect”. MIT Technology Review. https://www.technologyreview.com/2022/10/10/1060982/china-pandemic-cameras-surveillance-state-book/

### Appendix
---
**Full flowchart:**
<br>
![](Full_flowchart.png)

**Our GitLab repositories:**

[Click for Beas' GitLab repository](https://gitlab.com/bea-ap/aesthetic-programming)
<br>
[Click for Tillas' GitLab repository](https://gitlab.com/tilla-ap/aesthetic-programming)
<br>
[Click for Mathildes' GitLab repository](https://gitlab.com/aesthetic-programming9494918/aesthetic-programming)
<br>
[Click for Wilhelms' GitLab repository](https://gitlab.com/wilhelmyaya/aesthetic-programming)