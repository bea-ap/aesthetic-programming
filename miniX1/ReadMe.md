# MiniX1: Popsicle in the Sun

![](Skærmbillede_miniX1_.png)
<br>

Run the program [here](https://bea-ap.gitlab.io/aesthetic-programming/miniX1/index.html)
<br>
View the code [here](https://gitlab.com/bea-ap/aesthetic-programming/-/blob/main/miniX1/minix1sketch.js?ref_type=heads)

## Reflecting questions
**What have I produced?**

For my first miniX assignment, I have created a code that represents a popsicle and next to it a sun, that is constantly enlarged and reduced. In the `function setup()`, I have made a simple light blue background with the code reference:
`background(137, 207, 240);` 
I chose the color light blue as i wanted the background to represent the sky. For the popsicle itself, I have mostly used the function:
`rect()`
I used 5 variables instead of 4 to create soft corners so that the icecream would look more realistic.
For example, I used the following code reference to create the main shape of the popsicle:
`rect(660, 440, 30, 250, 20);`
I used the same function to create the stick and the white chocolate top. For the chocolate, however, I also used the reference `circle()` to make the top of the icecream round. Furthermore I used the same function to create sprinkles on the chocolate. Here I have used the reference multiple times and adjusted the 'x' and 'y' values ​​so that they were placed differently on the 'chocolate top'. 

To create the sun I used the reference: growing and shrinking
by amcc. I used this speciffic refference to create a 'living sun' and I to try my hand at an animation effect. 
To create this effect, if/else statements are used; If the diameter (dia) of the circle becomes greater than 200, grow is set to false. If the diameter becomes less than 150, grow is set to true.
Depending on the value of grow, the dia variable either increases or decreases by the value of growAmount.
In addition, I have also used the functions `stroke()` and `strokeWeight()` to create an orange ring around the center circle of the sun, to make it look more realistic.
Inside the function `stroke()`, I used the following RGB code to create the orange color:`(255,127,0)`.

**How is the coding process different from, or similar to, reading
and writing text?**

I think in general there are many differences between the coding process and reading and writing text. The coding process is difficult to understand, at least as a beginner because it is a completely new language to learn. There are many characters that mean certain things in the coding process which mean other things in normal writing contexts. It can also be said that the coding process has a specific purpose, namely to create something specific with code, where the purposes of writing and reading in general can be very different and there is not necessarily a specific purpose. 

In relation to where the two things resemble each other is, for example, syntax. Both coding and written language require adherence to syntax and grammar rules. In written language, you need to follow grammar rules for clarity and comprehension. Similarly, in coding, you must follow the syntax rules of the programming language to ensure the code is correct and understandable by the computer.

### **References**
[growing and shrinking by amcc](https://editor.p5js.org/amcc/sketches/3ZLqytY_4)
