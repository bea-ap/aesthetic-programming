let dia = 0;
let growAmount = 1;
let grow = true;

function setup() {
 // put setup code here
 createCanvas (windowWidth,windowHeight);
 background(137,207,240);
}
function draw() {
   //put drawing code here
   noStroke();
   fill(236,213,167); //color of the icecream stick 
   rect(660, 440, 30, 250, 20); //shape of the icecream stick 
   fill(200,25,0); //icecream color 
   rect(575, 200, 200, 350, 35); //shape of the icecream 
   fill(150,0,0); //color of the stroke on the icecream
   rect(615, 340, 35, 175, 30);// the stroke
   fill(150,0,0);
   rect(700, 340, 35, 175, 30);
   fill(252,255,234); //color of the choclate top of the icecream
   circle(675,250,202);//the shape of the choclate top 
   fill(252,255,234);//color of the choclate choclate top
   rect(575, 204, 200, 147, 30);//choclate top

   //sprinkles
  fill(24,25,26); //color
   circle(700,300,4);
   circle(740,320,4);
   circle(700,250,5);
   circle(650,250,4);
   circle(630,200,5);
   circle(610,230,4);
   circle(670,280,3);
   circle(650,250,4);
   circle(720,200,4);
   circle(600,300,5);
   circle(590,260,4);
   circle(630,330,4);
   circle(670,320,3);
   circle(720,290,4);
   circle(755,250,5);
   circle(635,300,4);
   circle(670,210,6);
   circle(680,170,4);
   circle(710,335,4);
   circle(630,265,3);
   circle(755,285,6);
  circle(730,230,4);

  //sun (growing and shrinking)
  stroke(255,127,0);
  strokeWeight(8);
  fill(255,244,0);
    circle(250,160,dia)
    if (dia > 200) {
      grow = false
    }
    if (dia < 150) {
      grow = true
    }
    
    if (grow == true) {
      dia += growAmount
    } else {
      dia -= growAmount
    }
    
    console.log("diameter", dia)
    console.log("grow", grow)
  }




