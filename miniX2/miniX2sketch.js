function setup() {
 // put setup code here
 createCanvas (windowWidth,windowHeight);
 background(245,245,220);
}
function draw() {

textSize(20);
noStroke();
text('Press to show inner feelings', 370, 600);
text('Who run the world?', 905, 245);

//#1 Happy Emoji
noStroke();
fill(255,224,51);
ellipse(500,400,290);

//Mouth
fill(185,103,60);
arc(500,435,130,115,0,PI);
fill (255,255,255);
rect(440,438,118,20,5);

//Cheeks
fill(225,170,203);
ellipse(410,390,30,15);
ellipse(585,390,30,15);

//Eyes
noFill();
stroke(204,102,0);
strokeWeight(5);
arc(455, 380, 45, 65,5*PI/4,7*PI/4);
arc(540, 380, 45, 65,5*PI/4,7*PI/4);

//(#1) Sad Emoji
  if (mouseIsPressed) {

//This makes the sad emoji only visble when the mouse is pressed
    noStroke();
    fill(255,224,51); 
    ellipse(500,400,290);

//Eyes
    noFill();
    stroke(204,102,0);
    strokeWeight(7);
    arc(450, 380, 55, 55,5*PI/4,7*PI/4);
    arc(550, 380, 55, 55,5*PI/4,7*PI/4);
    fill(120,213,227,156);
    noStroke();
    rect(435, 355, 23, 178, 20);
    rect(543, 355, 23, 178, 20);
   
//Mouth
    fill(185,103,60);
    ellipse(500, 460, 40, 45);
    noFill();
    fill(255,255,255);
    rect(487,440,26,20,10);
  }

//#2 Feminist emoji
//Top
  noStroke();
  fill(230,38,129);
  ellipse(1000,400,250);

//Body/cross
  stroke(230,38,129);
  strokeWeight(25);
  line(1000, 490, 1000, 630);
  line(1050, 570, 950, 570);

//Sunglasses
  noStroke();
  fill(26,13,0);
  ellipse(1050,370,90,70);
  ellipse(950,370,90,70);
  stroke(26,13,0);
  strokeWeight(6);
  line(1050,370,950,370);
  stroke(255,255,255);

//Mouth
  noFill();
  strokeWeight(5);
  arc(1000, 390, 160.8, 160, 	QUARTER_PI,PI-QUARTER_PI);
}

