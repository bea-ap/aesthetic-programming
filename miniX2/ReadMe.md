# MiniX2: Emojis & Emotions

![](miniX2.png)
![](miniX2_2_.png)
<br>

Run the program [here](https://bea-ap.gitlab.io/aesthetic-programming/miniX2)
<br>
View the code here [here](https://gitlab.com/bea-ap/aesthetic-programming/-/blob/main/miniX2/miniX2sketch.js)

## Reflecting questions
**Describe your program and what you have used and learnt.**

In this miniX assignment I have made two(three) emojis.

I started by adjusting the canvas size to `createCanvas(windowWidth,windowHeight)` so that i the whole window is viewed.
 
The first emoji represents a very happy face, after which it changes to a sad face if you press the mouse.To make it do this, I have used the p5 reference `mouseIsPressed`for the first time.
I have used `ellipse()` to create the frame for all of my emojis. In addition, I have used the `arc()` refference to create the mouth and eyes of the happy emoji as well as the eyes of the sad emoji. For example, I have used the following code reference for the mouth of the happy emoji:

`arc(500,435,130,115,0,PI);`

In particular, I developed a better understanding of this function in this assignment. I gained an understanding of how pi controls the shape of the circle.
I have also added small details such as teeth where I have used the reference `rect()` with 5 numbers to make soft corners and for the cheeks I have used `ellipse()`, also i have created running tears for the sad emoji with the `rect()`reference.

For my second emoji I have used the p5 refference `line()` to create the cross under the 'head' of the emoji.In addition I have made sunglasses also using the `line()` and `ellipse()` functions and for the mouth I have used the `arc()` reference. In addition, I have chosen to make it pink with the following RGB color code:`fill(230,38,129)`as I want it this emoji to be very feminine.

**How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on?**

The first emoji represents putting on a facade. Eventhough it can seem like you are very happy, in reality you are maybe not happy at all, and actually you spend most of your time being sad when you are not out and feel like you have to wear a mask. With the text 'press to show inner feelings' I have tried to describe that a person is much more than a happy face. There are a lot of feelings hidden inside a person that other people don't know anything about.

With the second emoji I have tried to create a focus on feminism, in a casual way. I think that feminism is often perceived in a wrong way, and that there is an impression formed that feminists are very extreme in their attitudes and that feminists hate men. I believe this is far from the truth and that feminism is much more about equality. So what I am trying to express by giving the emoji sunglasses is, for example, that it is cool to be a feminist and that there is no hatred in it. Also, it is of course generally cool to be a woman and support the female sex. 

### **References**
P5js.org "arc" reference

https://p5js.org/reference/#/p5/mouseIsPressed
