let angle = 0; // Angle for the moon's position
let moonSpeed = 0.02; // Speed of the moon's movement
let maxMoonSpeed = 0.06; // Maximum speed of the moon's movement
let numStars = 100; // Number of stars
let textToShow = "Loading..."; //Default text

// Arrays to store the positions of stars
let starX = [];
let starY = [];

function setup() {
  createCanvas(600, 1000);

  // Generating random positions for the stars
  for (let i = 0; i < numStars; i++) {
    starX[i] = random(width);
    starY[i] = random(height);
  }

  // Text change after 3 seconds (3000 milliseconds)
  setTimeout(changeText, 3000);
}

function draw() {
  background(5,20,31);

  // Stars
  fill(255);
  noStroke();
  for (let i = 0; i < numStars; i++) {
    ellipse(starX[i], starY[i], 2, 2); 
  }

  // Calculate the position of the earth based on the center of the ellipse
  let earthX = width / 2;
  let earthY = height / 2;

  // The text
  textAlign(CENTER, TOP);
  fill(255);
  textSize(20);
  text(textToShow, 285, 800); 

  // Draw the earth in the center of the canvas
  textSize(100);
  text('🌎', earthX - textWidth('🌎') / 5, earthY + textAscent() / 5); // Adjust

  // The moon's position based on the angle
  let moonX = width / 2 + 170 * cos(angle);
  let moonY = height / 2 + 140 * sin(angle);

  // The moon
  textSize(50);
  text('🌕', moonX - textWidth('🌕') / 2, moonY + textAscent() / 2);


  angle += moonSpeed; //Updates the angle variable by adding the value of moonSpeed 
}

function mousePressed() {
  moonSpeed = maxMoonSpeed; // Increasing the moon's speed when the mouse is pressed
}

function mouseReleased() {
  moonSpeed = 0.02; // Sets the moon's speed back to the original value when the mouse is released
}

function changeText() {
  if (textToShow === "Loading...") {
    textToShow = "Refresh to change position of the stars"; // Change the text to "refresh to change the stars"
  } else if (textToShow === "Refresh to change position of the stars") {
    textToShow = "Press mouse to speed up"; // Change the text to "press to speed up"
  } else {
    textToShow = "Loading..."; // Change the text back to "loading..."
  }

  // Schedule of the next text change with a 3-second delay
  setTimeout(changeText, 3000);
}