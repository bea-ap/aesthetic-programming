 # MiniX3: Moon&Earth Throbber
![](miniX3.gif)

<br>

Run the program [here](https://bea-ap.gitlab.io/aesthetic-programming/miniX3/index.html)
<br>
View the code [here](https://gitlab.com/bea-ap/aesthetic-programming/-/blob/main/miniX3/miniX3sketch.js)

## Describtion of my Throbber and the code
My Throbber is representing the moon's orbit around the earth. The 'thinking process' is expressed in the moon traveling around the earth. 

My code starts by declaring various variables such as `let moonSpeed` and `let numStars`. I do this as it helps organizing the code structure, and also by declaring variables at the top, they are given global scope, meaning they can be accessed and modified from anywhere within the code. Keeping variables like `moonSpeed` and `numStars` in global scope ensures that their values persist throughout the entire runtime of the program. 
I also create arrays for the stars which acts as a part of the background. Using arrays allows me to store multiple data points (`x` and `y` positions of stars) in a structured way, instead of declaring separate variables for each star. In the setup function, I then use the following `for loop` to create random positions for the stars:

`for (let i = 0; i < numStars; i++) {
    starX[i] = random(width);
    starY[i] = random(height);
  }`

To make the earth and the moon itself, I inserted an emoji of the earth and the moon in each `text` function. I use `textsize` to adjust the size of the emojis. 
I use the function `changeText` and the callback`setTimout` to make the text change after 3 seconds: `setTimeout(changeText, 3000);`, here the number 3000 represents 3000 milliseconds.
In addition, I use an if-else statement in the `changeText` function to change the value of the `textToShow` variable depending on its current value.
Furthermore I use the function `mousePressed`and `mouseReleased` to adjust the speed of the moon. At the beginning of the code i have declared the variable `let maxMoonSpeed = 0.06;` which means that the maximum speed is already defined and I can therefore just write the following code: 
`mousePressed() {moonSpeed = maxMoonSpeed}`.

The same principle applies when I use `mouseReleased`. Here I have defined moonSpeed ​​to the value `0.02` at the beginning of my code, and thus the program knows what speed it should go back to when the mouse is released.

### Reflecting questions
**What do you want to explore and/or express?**

I have always found space to be exciting and in space there are many different systems and astronomical phenomena that take place over time. In combination of these things, the idea for my throbber arose. I have attempted to design a throbber that is engaging enough to prevent impatience and frustration. I have done this by, for example, letting the text change and by adding the function where you can change the speed of the moon.

**Think about a throbber that you have encounted in digital culture, e.g. for
streaming video on YouTube or loading the latest feeds on Facebook, or waiting
for a payment transaction, and consider what a throbber communicates, and/or
hides? How might we characterize this icon differently?**

I think Throbbers mainly communicate ongoing activity and it expresses that the system is working on a task and has not frozen or crashed. It provides reassurance to the user that progress is being made. Also Throbbers often hide the exact progress of the task being performed, they simplify the representation of potentially complex background processes into a single, easily recognizable animation. While they indicate activity, they often don't provide information on how much longer the process will take or what specific steps are being completed, and this, I think, can easily create impatience and frustration. In addition, I think that there often is a connection between the website the throbber appears on and the throbber itself. For example, if I shop for clothes online, the throbber can illustrate a shopping purchase or something similar. I think this creates a good transition and you also have a sense that you are waiting for the "right" thing.

#### Referrences
https://p5js.org/reference/#/p5.Element/mousePressed

https://p5js.org/reference/#/p5.Element/mouseReleased

https://editor.p5js.org/xinemata/sketches/dsg1Pni8k

https://www.youtube.com/watch?v=ZfFtXzzTwP8


