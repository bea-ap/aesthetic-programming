# MiniX9: Flowchart
For this miniX I have made a flowchart based on the code for my miniX4. 
<br>
**Run the code for my miniX4** [here](https://bea-ap.gitlab.io/aesthetic-programming/miniX4/index.html)
<br>
**View the code for my miniX4** [here](https://gitlab.com/bea-ap/aesthetic-programming/-/blob/main/miniX4/miniX4sketch.js)

![](flowchart.png)

## What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?
I find it difficult to create a good balance between the complex algorithmic level and the simpler, communicative level. Generally, I find it challenging to explain code in a simple and easy way because it is very abstract, and there are many things happening in code that are super difficult to explain in a simple manner. You also need to be careful not to make it too simple, such that important elements, as the code does, are omitted. Overall, I don't think it was super easy to create a flowchart of my code, and i found it difficult to decide how much detail to include, however it definitely contributed to a better overview of the code.

## In which way are the individual flowcharts you produced useful?
I think it has been super useful to create a flowchart based on my previous miniX. It has been useful because in order to make it, you have to fully understand the code and understand what each step in the code does and that definitely helps to provide a much better understanding of the code and what it exactly does when the program runs.