// Defining variables I use in the code
let capture; 
let mic;
let button;
let showFane = true; //indicates that the fane is shown as standard when the program starts
let showCapture = false; //indicates that capture should not be shown when the program starts, only when the user interacts with the button

function setup() {
  createCanvas(640, 480);

// Creating audio input
 mic = new p5.AudioIn();

  // Creating 'Accept' button 
  button = createButton("Accept");
  button.style("background", "rgb(225, 255, 225)")
  button.style("color", "rgb(0)")
  button.style("border-radius", "10px"); // 10px border radius for soft corners
  button.size(100, 30)
  button.position((width - 100) / 2, (height + 50) / 2);
  button.mousePressed(change); //calls the change function when the button is pressed
}

  // Creating and positioning the fane
function fane() {
  let rectWidth = 204; 
  let rectHeight = 100; 
  let rectX = (width - rectWidth) / 2;
  let rectY = (height - rectHeight) / 2;

// The shape of the fane
  stroke(0);
  fill(211, 211, 211);
  let borderRadius = 6;
  rect(rectX, rectY, rectWidth, rectHeight, borderRadius);

// Fane text
  fill(0);
  textSize(14);
  textAlign(CENTER, CENTER);
  let textY = rectY + rectHeight / 2 - 15;
  text('Accept Terms & Conditions', rectX + rectWidth / 2, textY);
}

function draw() {
  background(255);

  // Tracking sound in the console log
console.log(mic)

  // Checks if 'showcapture' is true
  if (showCapture) {
    // Shows screenrecording if 'Showcapture' is true
    if (!capture) {
      capture = createCapture(VIDEO);
      capture.size(640, 480);
      capture.hide();
    }
    image(capture, 0, 0, width, height);

    // Drawing 'Thank You' text
    textFont('monospace');
    textSize(20);
    textAlign(CENTER, TOP);
    fill(217, 33, 33);
    text('Thank you for letting us use all your data!😈', width / 2, height / 2-160);
    
    // Drawing and positioning volumebar
    let vol = mic.getLevel();
    fill(255,0,0); 
    rect(50, height-50, 15, -vol * 1000); 
    
    // Text under the volumebar
    textStyle(BOLD);
    textSize(15);
    textAlign(CENTER);
    fill(255,0,0);
    text('MIC 🔴', 65, height - 35);

  } else {
    // Shows fane if 'showFane' is true
    if (showFane) {
      fane();
    }
  }
}
// Function called when the button is pressed
function change() {
    // Starting sound capture when the button is pressed
    mic.start();  
  //showFane changes to false, it is remowed when the button is pressed
  showFane = false;
  // showCapture changes to true, showing screenrecording 
  showCapture = true;
  // Hides button after it is pressed
  button.hide();

}
