# MiniX4: What did i just Accept?
![](miniX4.png)

<br>
Run the code [here](https://bea-ap.gitlab.io/aesthetic-programming/miniX4/index.html)
<br>
View the code [here](https://gitlab.com/bea-ap/aesthetic-programming/-/blob/main/miniX4/miniX4sketch.js)

## Title and description of my program

My MiniX4 "What Did I Just Accept?" delves into the common scenario where users often skim through or just ignore terms and conditions when visiting a website. The "accept" button is often pressed without further thoughts on what is actually accepted. My program shows a window where you have to accept terms and conditions to be able to move on. The button is green, which encourages the user to click it. Suddenly, the message "Thank you for letting us use all your data" appears on the screen, accompanied by the activation of the webcam and microphone. Unknowingly the user have given the program permission to access and utilize their data, without knowing that this was what they agreed to when accepting the terms and conditions. In this way my project is a kind of caricature, that focuses on our casual acceptance of terms and conditions being unaware of what is actually accepted.

### What I have used and learnt

For this miniX assigment I have learned and used new syntaxes such as p5.sound and Video capture, also I have used the DOM element `createbutton`. and focused on customizing it's appearance. In addition I have worked with if statements to control when the various elements should be visible and not visible.

#### How my program and thinking address the theme of “capture all.”

My program relates to the topic 'capture all' by illustrating how all of one's data can be swiftly 'captured' in a split second, often without the user's awareness. It delves into how certain websites can easily obtain access to an individual's personal data, leaving the user suddenly out of control over how their personal information is utilized and potentially exploited. It also addresses the user's lack of awareness in relation to how much and which data is being 'captured'.

##### References

Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119
https://p5js.org/reference/#/p5/createCapture
